<?php
  /*

  PROBLEMS:
  * Writing HTML in an echo seems sloppy, it should probably be done with JQuery in index.php, tough it nets the same result.

  */

  //POST DATA FROM INDEX
  $postcat = $_POST['category'];

  //IF CATEGORY IS null
  if ($postcat == 'null') {
    echo "<option>Select a Category</option>";
  }
  else {
    //parse JSON
    include 'json.php';

    foreach($data[$postcat] as $index=>$val) {
      //define option name
      $name = $val['name'];
      //echo options with name and index, index is set for gen.php to use when generating the tag
      echo "<option value='$index'>$name</option>";
    }
  }
?>