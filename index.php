<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>MetaGen</title>
  <!--Meta-->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!--Jquery-->
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>

  <!--Get Select2-->
  <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/css/select2.min.css" rel="stylesheet" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0-rc.2/js/select2.min.js"></script>

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
  
  <link rel="stylesheet" href="res/css/codefont/font.css">
  <link rel="stylesheet" type="text/css" href="res/css/style.css">

</head>
<body>
  <nav class="navbar navbar-default">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <a class="navbar-brand" style="padding: 10px" href="#"><h1 class="logo"><span class="glyphicon glyphicon-cog"></span>meta<span class="color">gen</span></h1></a>
      </div>
    </div><!-- /.container-fluid -->
  </nav>

  <div class="container">
    <form id="form" action="gen.php" type="GET">
      <section class="row vspace-2" id="gen">
        <h2 class="col-sm-12 vspace-2">Generate Meta Tags</h2>

        <div id="c" class="col-sm-4 vspace-1 col-xs-6">
          <label for="category">Meta Category</label>
          <select name="cat" id="category" size="2">
          <option value="" disabled selected>Select a category</option>
            <?php
              //JSON 
              $object = "objects.json";
              $json = file_get_contents("$object");
              $data = json_decode($json, true);
              foreach($data as $key=>$val){
                foreach($val as $k=>$v){
                  echo "<option value='$k'>$k</option>";
                }
              }
            ?>
          </select>
        </div>
        
        <div id="t" class="col-sm-4 vspace-1 col-xs-6">
          <label for="type">Meta Tag</label> 
          <select id="type" name="type"  size="2">
            <option value="" disabled selected>Select a tag</option>
          </select>
        </div>
        
        <div id="n" class="col-sm-4 vspace-1 col-xs-12">
          <label for="nested">Properties</label>
          <select id="nested" name="nested[]" multiple disabled>
          </select>
        </div>
        
        <div class="col-sm-4 col-xs-12">
          <label for="content">Manual Meta Content</label>
          <textarea class="form-control" placeholder="Optional..." id="content" name="content"></textarea>
        </div>
      </section>
      
      <div class="col-sm-12 vspace-3">
        <input class="btn btn-primary btn-lg" id="submit" type="submit" value="Generate">
      </div>
    </form>
    
    <section class="row">
      
      <div class="col-sm-12 col-xs-12 copybox">
        <label for="#copybox">Generated Tags</label>
        <pre><code id="copybox"></code></pre>
      </div>

      <div class="col-sm-12">
        <div class="btn-group btn-group-justified vspace-3">
          <div class="btn-group">
            <button class="btn btn-default" id="clear">Clear Box</button>
          </div>
          <div class="btn-group">
            <button class="btn btn-default" id="raw"><span class=""></span>Raw Code</button>
          </div>
          <div class="btn-group">
            <button class="btn btn-info dl" id="saveHTML" style="height: 34px;">Save to HTML</button>
          </div>
        </div>
      </div>
    </section>

  </div><!--Container-->

  <footer>
    <h1 class="logo lg"><span class="glyphicon glyphicon-cog"></span>meta<span class="color">gen</span></h1>
  </footer>


<!--Init-->
<script type="text/javascript">
  //JQuery plugin to make selection-boxes look better!
  $('select').select2();

  //Smooth Scroll
  $(function() {
    $('a').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 600);
          return false;
        }
      }
    });
  });

  //Cool BG Effect
  $('footer').mousemove(function(e) {
    var x = (e.pageX * -1 / 15), y = (e.pageY * -1 / 15);
    $(this).css('background-position', x + 'px ' + y + 'px');
  });
</script>

<script>
//Category AJAX

//When the category changes
$('#category').change(function (e) {
    //prevent default behaviour, if there is any. this is more of a saftey measure for HTML forms
    e.preventDefault();
    //do an AJAX request
    $.ajax({
        //what script to run
        url: 'load.php',
        //how to send the data, POST or GET, they behave differently. in this case POST!
        type: 'POST',
        //data to send to the script, in this case the selected category
        data: {category:$('#category option:selected').val()},
        //on success
        success: function(data) {
          //Add options to #type selector
          $("#type").html(data);
        }
    });
});

//Object Options AJAX
$('#type').change(function (n) {
    n.preventDefault();
    $.ajax({
        url: 'nested.php',
        type: 'POST',
        data: {option:$('#type option:selected').val(), category:$('#category option:selected').val()},
        success: function(data) {
          $("#nested").html(data);
          if($("#nested:contains('No Properties')").length) {
            $("#n").removeClass("noprops");
            $("#n").addClass("normal");
            $("#nested").attr('disabled', true);
          } else {
            $("#nested").attr('disabled', false);
            $("#n").addClass("noprops");
            $("#n").removeClass("normal");
          }
        }
    });
});

//Generate AJAX
$("#form").on("submit", function(f) {
    f.preventDefault();
    $.ajax({
        url: $(this).attr("action"),
        type: 'POST',
        data: $(this).serialize(),
        success: function(data) {
          $("#content").val('');
          $("#copybox").append(data);
        }
    });
});


//Clearing
$("#c").click(function() {
  //set value of boxes to the left to null
  $("#type").select2('val', null);
  $("#nested").select2('val', null);
});

$("#t").click(function() {
  //set value of boxes to the left to null
  $("#nested").select2('val', null);
});

//TOOLBAR
$("#clear").click(function() {
  //remove html in copybox
  $("#copybox").html('');

    //restore download button if pressed
    $(".dl").html("Save to HTML");
    $('.dl').attr('id', 'saveHTML');
    if($(".dl").attr("id")) {
      $(".dl").removeClass("btn-success");
      $(".dl").addClass("btn-info");
    }
});

//raw code
$("#raw").click(function() {
    var w = window.open();
    var html = $("#copybox").html();

    // how do I write the html to the new window with JQuery?
      $(w.document.body).html(html);
});

//save to HTML
$("#saveHTML").click(function() {
  $.ajax({
      url: 'html.php',
      type: 'POST',
      data: {generate:$('#copybox').html()},
      beforeSend: function() {
        $("#saveHTML").html('<div class="loader"></div>');
      },
      success: function(data) {
        setTimeout(function() {
          $("#saveHTML").html(data);
          $("#saveHTML").removeClass("btn-info");
          $("#saveHTML").addClass("btn-success");
          $('#saveHTML').removeAttr('id');
        }, 1500); //Yes, it is a timeout function, I just wanted to try out some loading animations!
      }
  });
});

</script>

</body>
</html>