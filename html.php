<?php
	/*

	PROBLEM:
	These files are not deleted upon download and are stored on the server/host computer forever.
	To circumvent this problem you create a "cronjob" on the host, an operation that runs on a specified interval.

	Sugested cronjob (if linux host):
	30 4 * * * rm /path/to/folder/*.html

	This would remove everything ending with ".html" in the specified path, every day at 04:30.

	A more secure way of removing files is to timestamp them and have the cronjob execute PHP-code that detects the timestamped files
	and only deletes files OLDER than an hour.
	
	*/

	//POST data from index
	$postgenerated = $_POST['generate'];

	//randomize a filename using a unique md5-string
	$rand = md5(uniqid(rand(), true));

	//set folder and filename
	$folder = "generated";
	$file = $rand.'.html';
	$open = $folder.'/'.$file;

	//open specified folder and file in write-mode, or die
	$myfile = fopen($open, "w") or die("Not able open file!");

	//txt to write
	$txt = $postgenerated;

	//remove broken tags
	$txt = str_replace("Tag does not exist.","", $txt);

	//decode html enteties generated in gen.php replace HTML <br> with a regular linebreak
	$txt = html_entity_decode(str_replace("<br>","\n", $txt));

	//write HTML to specified file, then close to stop writing.
	fwrite($myfile, $txt);
	fclose($myfile);

	//echo downloadable URL, setting the downloaded filename to something more readable.
	$url = "<a href='$open' download='Meta.html'>Download</a>";
	echo $url;

?>