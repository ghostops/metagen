<?php
  /*

  PROBLEMS:
  * Ignoring JSON index errors is not the best way of doing things, since the error is still there, just not reported.
    I don't have a good fix for this in mind, but checking IF there is an index before declaring it might suppres the error.
  * Could replace the elseif statements during generation with a switch, tough it would ultimatley net the same result wihtout 
    any preformance loss.

  */

  //ignore index errors related to JSON index
  error_reporting( error_reporting() & ~E_NOTICE );

  //POST DATA FROM INDEX
    //type of meta generated
    $postindex = $_POST['type'];
    //type category
    $postcat = $_POST['cat'];
    //IF content is manually written
    $postcontent = $_POST['content'];
    //if the DEFAULT content is nested
    $postnested = $_POST['nested'];

  //Parse JSON
  include 'json.php';

  //GET JSON DATA
    //post type [e.g. "meta" or "link"]
    $pt = $data[$postcat][$postindex]['type'];

    //post name [e.g. "author" or "viewport"]
    $pn = $data[$postcat][$postindex]['name'];

    //post content
      //if MANUAL postcontent is NOT written
      if ( empty($postcontent) ) {
        //pc equals JSON content
        $pc = $data[$postcat][$postindex]['content'];
        
        //if JSON content is an array
        if (is_array($pc)) {
          //if the array is sent empty
          if (empty($postnested)) 
            $pc = "n/a";
          else
            //pc equals imploded array, each item separated by ', ' 
            $pc = implode(", ", $postnested);
        }
      }
      else
        //pc equals MANUAL postcontent
        $pc = $postcontent;

    //favicon specific data
    $it = $data[$postcat][$postindex]['icon-type'];


  //GENERATE TAG
    //based on meta-type response from JSON
    if ($pt == "meta") {
      $generated = "<$pt name='$pn' content='$pc'>";
    }
    elseif ($pt == "rel") {
      $generated = "<link rel='$it' href='$pc' type='$pn'>";
    }
    elseif ($pt == "og") {
      $generated = "<meta property='$pn' content='$pc'>";
    }
    elseif ($pt == "cdnJS") {
      $generated = "<script src='$pc'></script>";
    }
    elseif ($pt == "cdnCSS") {
      $generated = "<link rel='stylesheet' href='$pc'>";
    }
    else {
      //If generated empty or otherwise faulty
      $generated = "Tag does not exist.";
    }
    //If nothing is generated, exit script
    if (empty($generated)) {
      exit();
    }

  //using HTML-enteties to escape HTML-parsing when displayed in index
  $generated = htmlentities($generated);

  //Echo generated tag!
  echo "$generated<br>";
?>