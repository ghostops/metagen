index.php - application UI and startpage (JQuery code is at the bottom)

gen.php - generation of the tags
load.php - load selected categorys tags
json.php - included in other PHP-files to parse JSON-data
nested.php - if tag has nested content, run this
html.php - generates HTML on buttonpress

objects.json - JSON data with meta tags to be generated

Written in PHP and JQuery by Ludvig Larsendahl
Site can be seen live at: http://ghostops.nu/metagen
